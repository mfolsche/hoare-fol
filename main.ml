(***
Implementation of the weakest precondition calculus using Hoare logic
on biological regulatory networks.

See the README file for usage notes.
Please refer to the technical report for documentation:
  M. Folschette,
  The Hoare-fol Tool,
  technical report, 2019.


Principle:
  This implementation defines grammars and trees to represent properties on
  networks and their dynamical states (such as “a=1 AND b>0 OR ...”) and a
  simple branching imperative language (increment, decrement, conditional, loop
  and quantifiers) with the aim of computing the weakest precondition from given
  postcondition and path program, on a given network.
  This weakest precondition can also be translated to Answer Set Programming
  (ASP) in order to be solved by Clingo 5.

Reference article:
  G. Bernot, J.-P. Comet, Z. Khalis, A. Richard, O. Roux,
  A genetically modified Hoare logic,
  Theoretical Computer Science, 2018.
  ISSN: 0304-3975
  DOI: https://doi.org/10.1016/j.tcs.2018.02.003

Useful functions:
  - wp: compute the weakest precondition
  - simplify: perform formula simplification
  - string_of_formula & string_of_prog_indent: pretty-print a formula or program
  - write_example: translate a formula into ASP
  - asp_params: print the correspondence between ASP variables and parameters

TODO:
  * Model & processes should not be hard-coded in this file
  * Purify all functions regarding the model
  * Make a weakest While loop invariant calculus
  * Finish processings on FreshState (simplifications, etc.)
***)



(************************)
(*** Common functions ***)
(************************)

open List ;;

(** Identity function **)
let id x = x ;;

(** Convert a list into a string with given separator and element-to-string converter *)
let rec string_of_list conv sep l =
  match l with
    | [] -> ""
    | h :: [] -> (conv h)
    | h :: t -> ((conv h) ^ sep ^ (string_of_list conv sep t)) ;;

let string_of_list_delim conv sep delim1 delim2 l =
  delim1 ^ string_of_list conv sep l ^ delim2 ;;

(** Compute the power set *)
let powerset l =
  let rec ps lp l =
    match l with
      | [] -> [lp]
      | h :: t -> (ps lp t) @ (ps (lp @ [h]) t)
in
  ps [] l ;;

(** Compute the oredered power set *)
let powerset_sort l fcompare =
  let rec ps lp l fcompare =
    match l with
      | [] -> [sort fcompare lp]
      | h :: t -> (ps lp t fcompare) @ (ps (lp @ [h]) t fcompare)
in
  ps [] l fcompare ;;

(** Get the index of an element in a list *)
let index l a =
  let rec index_rec l a n =
    match l with
      | [] -> raise Not_found
      | h :: t -> if h = a then n else index_rec t a (n+1)
in
  index_rec l a 0 ;;

(** Option type for search in associative lists *)
type assoc_option =
  | Found of int
  | Not_found ;;



(*********************************************)
(*** Common logic and arithmetic operators ***)
(*********************************************)

(** Logical operators *)
type propop2 =
  | And | Or | Impl ;;
type propop1 =
  | Neg ;;
type propop0 =
  | True | False ;;

(** Comparison and arithmetic operators *)
type relop2 =
  | LE | LT | GE | GT | Eq | NEq ;;
type exprop2 =
  | Plus | Minus ;;

(** Convert an operator into ASP *)
let asp_of_relop2 = function
  | LE -> "<="
  | LT -> "<"
  | GE -> ">="
  | GT -> ">"
  | Eq -> "=="
  | NEq -> "!=" ;;
let asp_of_exprop2 = function
  | Plus -> "+"
  | Minus -> "-" ;;

(** Convert an operator into a pretty-printable string *)
let string_of_propop2 = function
  | And -> "∧"
  | Or -> "∨"
  | Impl -> "=>" ;;
let string_of_propop1 = function
  | Neg -> "¬" ;;
let string_of_propop0 = function
  | True -> "true"
  | False -> "false" ;;
let string_of_relop2 = function
  | LE -> "≤"
  | LT -> "<"
  | GE -> "≥"
  | GT -> ">"
  | Eq -> "="
  | NEq -> "≠" ;;
let string_of_exprop2 = function
  | Plus -> "+"
  | Minus -> "−" ;;



(**********************)
(*** Types of a BRN ***)
(**********************)

(** Types **)
type var = string ;;
type mult = string ;;
type varmax = int ;;
type varpredec = mult list ;;

(** Comparison functions **)
let varequals = (=) ;;
let multcompare = String.compare ;;



(**************************************)
(*** Grammar for multiplex formulas ***)
(**************************************)

(** Formulas and arithmetic expressions of multiplexes *)
type multexpr =
  | MExprBin of exprop2 * multexpr * multexpr
  | MExprConst of int ;;
type multformula =
  | MPropConst of propop0
  | MPropUn of propop1 * multformula
  | MPropBin of propop2 * multformula * multformula
  | MRel of relop2 * multexpr * multexpr
  | MAtom of var * int
  | MMult of mult ;;



(***********************)
(*** BRN description ***)
(***********************)

(** Types: info on variables and multiplexes *)
(* vars must be a list of ( var   * (varmax *  varpredec )) *)
(*               that is: (string * ( int   * string list)) *)
type varinfo = (var * (varmax * varpredec)) list ;;
(* mults must be a list of ( mult  * multformula) *)
(*                that is: (string * multformula) *)
type multinfo = (mult * multformula) list ;;

(** Main article's toy example *)
let vars =
  [("a", (1, [])) ;
   ("b", (1, ["lambda" ; "sigma"])) ;
   ("c", (1, ["l"]))] ;;
let mults =
  [("l", MAtom("a", 1)) ;
   ("lambda", MPropUn(Neg, MAtom("c", 1))) ;
   ("sigma", MAtom("a", 1))] ;;

    
    
(** Other examples *)
(*
(** Toy example *)
let vars : varinfo = [("A", (1, ["M"]))] ;;
let mults : multinfo = [("M", MPropUn(Neg, MAtom("A", 1)))] ;;

(** Lambda phage (from Adrien Richard's PhD thesis) *)
let vars : varinfo =
  [("CI", (2, ["M1" ; "M2" ; "M5"])) ;
   ("Cro", (3, ["M3" ; "M4"])) ;
   ("CII", (1, ["M3" ; "M4" ; "M8"])) ;
   ("N", (1, ["M6" ; "M7"]))] ;;
let mults : multinfo =
  [("M1", MAtom("CI", 2)) ;
   ("M2", MPropUn(Neg, MAtom("Cro", 1))) ;
   ("M3", MPropUn(Neg, MAtom("CI", 2))) ;
   ("M4", MPropUn(Neg, MAtom("Cro", 3))) ;
   ("M5", MAtom("CII", 1)) ;
   ("M6", MPropUn(Neg, MAtom("CI", 1))) ;
   ("M7", MPropUn(Neg, MAtom("Cro", 2))) ;
   ("M8", MAtom("N", 1))] ;;

(** Circadian cycle (from the Circlock project)  *)
let vars : varinfo =
  [("PERCRY", (1, ["PC"])) ;
   ("PER1", (1, ["ClockBMAL"])) ;
   ("PER2", (1, ["ClockBMAL" ; "Context"])) ;
   ("CRY1", (1, ["CBR"])) ;
   ("CRY2", (1, ["ClockBMAL"])) ;
   ("Clock", (1, [])) ;
   ("RevErbAlpha", (1, ["ClockBMAL"])) ;
   ("BMAL1N", (1, ["Inhib"])) ;
   ("BMAL1C", (1, ["Acetylation"]))] ;;
let mults : multinfo =
  [("PC", MPropBin(And,
              MPropBin(Or, MPropBin(Or, MMult("PER1PER1"), MMult("PER1PER2")), MMult("PER2PER2")),
              MPropBin(Or, MPropBin(Or, MMult("CRY1CRY1"), MMult("CRY1CRY2")), MMult("CRY2CRY2")))) ;
   ("PER1PER1", MAtom("PER1", 1)) ;
   ("PER1PER2", MPropBin(Or, MAtom("PER1", 1), MAtom("PER2", 1))) ;
   ("PER2PER2", MAtom("PER2", 1)) ;
   ("CRY1CRY1", MAtom("CRY1", 1)) ;
   ("CRY1CRY2", MPropBin(Or, MAtom("CRY1", 1), MAtom("CRY2", 1))) ;
   ("CRY2CRY2", MAtom("CRY2", 1)) ;
   ("ClockBMAL", MPropBin(And, MPropBin(And, MAtom("Clock", 1), MAtom("BMAL1N", 1)), MPropUn(Neg, MAtom("PERCRY", 1)))) ;
   ("CBR", MPropBin(And, MPropUn(Neg, MAtom("RevErbAlpha", 1)), MMult("ClockBMAL"))) ;
   ("Inhib", MPropUn(Neg, MAtom("RevErbAlpha", 1))) ;
   ("Acetylation", MPropBin(And, MAtom("BMAL1C", 1), MAtom("Clock", 1))) ;
   ("Context", MPropConst(True))] ;;
*)



(***********************************)
(*** Other translation functions ***)
(***********************************)

(** Translations to string **)
let asp_of_var = String.capitalize_ascii ;;
let asp_of_mult = String.capitalize_ascii ;;
let string_of_var = id ;;
let string_of_mult = id ;;

(** Extract variables, multiplexes, etc. from the model **)
let varlist = fst (split vars) ;;
let multlist = fst (split mults) ;;
let getbound a = fst (assoc a vars) ;;
let getpredec a = snd (assoc a vars) ;;
let getformula m = assoc m mults ;;

(** Convert a multiplex arithmetic expression or formula into a pretty-printable string *)
let rec string_of_multexpr = function
  | MExprBin(op, e1, e2) -> ("(" ^ (string_of_multexpr e1) ^ (string_of_exprop2 op) ^ (string_of_multexpr e2) ^ ")")
  | MExprConst(i) -> string_of_int i ;;
let rec string_of_multformula = function
  | MPropConst(op) -> string_of_propop0 op
  | MPropUn(op, f) -> ((string_of_propop1 op) ^ (string_of_multformula f))
  | MPropBin(op, f1, f2) -> ("(" ^ (string_of_multformula f1) ^ " " ^ (string_of_propop2 op) ^ " " ^ (string_of_multformula f2) ^ ")")
  | MRel(op, e1, e2) -> ("(" ^ (string_of_multexpr e1) ^ (string_of_relop2 op) ^ (string_of_multexpr e2) ^ ")")
  | MAtom(v, i) -> ((string_of_var v) ^ "_" ^ (string_of_int i))
  | MMult(m) -> string_of_mult m ;;



(***************************************)
(*** Grammar of Hoare logic formulas ***)
(***************************************)

(** Artihmetic expressions and general formulas (for Hoare triples and program conditionals) *)
type expr =
  | ExprBin of exprop2 * expr * expr
  | ExprVar of var
  | ExprParam of var * (mult list)
(*| ExprVal of string (* TODO: Variables exterior to a Hoare triple? *) *)
  | ExprConst of int ;;
type formula =
  | PropConst of propop0
  | PropUn of propop1 * formula
  | PropBin of propop2 * formula * formula
  | Rel of relop2 * expr * expr
  | FreshState of formula ;;

(** Convert a multiplex formula into a general formula *)
let formula_of_matom v i = Rel(GE, ExprVar v, ExprConst i) ;;
let rec expr_of_multexpr = function
  | MExprBin(op, e1, e2) -> ExprBin(op, expr_of_multexpr e1, expr_of_multexpr e2)
  | MExprConst(i) -> ExprConst(i) ;;
let rec formula_of_multformula = function
  | MPropConst(op) -> PropConst(op)
  | MPropUn(op, f) -> PropUn(op, formula_of_multformula f)
  | MPropBin(op, f1, f2) -> PropBin(op, formula_of_multformula f1, formula_of_multformula f2)
  | MRel(op, e1, e2) -> Rel(op, expr_of_multexpr e1, expr_of_multexpr e2)
  | MAtom(v, i) -> formula_of_matom v i
  | MMult(m) -> formula_of_multformula (getformula m) ;;

(** Convert an arithmetic expression into ASP *)
let rec asp_of_state_expr s =
  let paramlist = fold_right (fun v l -> (powerset_sort (getpredec v) multcompare) :: l) varlist []
in function
  | ExprBin(op, e1, e2) -> ("(" ^ (asp_of_state_expr s e1) ^ (asp_of_exprop2 op) ^ (asp_of_state_expr s e2) ^ ")")
  | ExprVar(v) -> (if s = 0 then "" else "S" ^ (string_of_int s) ^ "X") ^ (asp_of_var v)
  | ExprParam(v, l) -> ("K_" ^ asp_of_var v ^ "_" ^ (string_of_int (index (nth paramlist (index varlist v)) (sort multcompare l))))
  | ExprConst(i) -> string_of_int i ;;
let asp_of_expr = asp_of_state_expr 0 ;;

(** Convert an arithmetic expression into a pretty-printable string *)
let rec string_of_expr = function
  | ExprBin(op, e1, e2) -> ("(" ^ (string_of_expr e1) ^ (string_of_exprop2 op) ^ (string_of_expr e2) ^ ")")
  | ExprVar(v) -> string_of_var v
  | ExprParam(v, l) -> ("k_" ^ string_of_var v ^ ",{" ^ (string_of_list string_of_mult ";" l) ^ "}")
  | ExprConst(i) -> string_of_int i ;;
let rec string_of_formula = function
  | PropConst(op) -> string_of_propop0 op
  | PropUn(op, f) -> ((string_of_propop1 op) ^ (string_of_formula f))
  | PropBin(op, f1, f2) -> ("(" ^ (string_of_formula f1) ^ " " ^ (string_of_propop2 op) ^ " " ^ (string_of_formula f2) ^ ")")
  | Rel(op, e1, e2) -> ("(" ^ (string_of_expr e1) ^ (string_of_relop2 op) ^ (string_of_expr e2) ^ ")")
  | FreshState(f) -> ("∀ state σ, " ^ (string_of_formula f)) ;;

(** Substitute an arithmetic expression for a variable *)
let replace f v e =
  let rec replace_expr ex v' e' =
    match ex with
      | ExprBin(op, e1, e2) -> ExprBin(op, replace_expr e1 v' e', replace_expr e2 v' e')
      | ExprVar(v) -> if (varequals v v') then e' else ExprVar(v)
      | ExprParam(v, l) -> ExprParam(v, l)
      | ExprConst(i) -> ExprConst(i) in
  let rec replace_formula tree v' e' =
    match tree with
      | PropConst(op) -> PropConst(op)
      | PropUn(op, f) -> PropUn(op, replace_formula f v' e')
      | PropBin(op, f1, f2) -> PropBin(op, replace_formula f1 v' e', replace_formula f2 v' e')
      | Rel(op, e1, e2) -> Rel(op, replace_expr e1 v' e', replace_expr e2 v' e')
      | FreshState(f) -> FreshState(f)
in
  replace_formula f v e ;;



(**********************************)
(*** Simplification of formulas ***)
(**********************************)

(* If available, variable and parameter values can be provided as two associative lists.
In this case, all these vales are be replaced for a more thorough simplification.
If no explicit variable or parameter value is known, empty lists are to be provided. *)

(** Simplify an arithmetic expression *)
let rec simpl_expr e lv lk =
  (* Search in the associative list *)
  let rec assoc_find eq k1 l =
    match l with
      | [] -> Not_found
      | (k2, v) :: t -> match eq k1 k2 with
                          | true -> Found v
                          | false -> assoc_find eq k1 t in
  (* Parameters equality *)
  let paramequals = (=) in
  (* Arithmetic expression evaluation *)
  let evalfun_of_exprop2 = function
    | Plus -> (+)
    | Minus -> (-)
in
(* Main arithmetic expression simplification *)
  match e with
    | ExprBin(op, e1, e2) -> (match simpl_expr e1 lv lk, simpl_expr e2 lv lk with
                                | ExprConst(i1), ExprConst(i2) -> ExprConst(evalfun_of_exprop2 op i1 i2)
                                | es1, es2 -> ExprBin(op, es1, es2))
    | ExprVar(v) -> (match assoc_find varequals v lv with
                       | Found i -> ExprConst(i)
                       | Not_found -> e)
    | ExprParam(v, l) -> (match assoc_find paramequals (v, l) lk with
                            | Found i -> ExprConst(i)
                            | Not_found -> e)
    | ExprConst(i) -> ExprConst(i) ;;

(** Simplify a formula *)
let rec simpl_formula f lv lk =
  (* Logic comparison evaluation *)
  let evalfun_of_relop2 = function
    | LE -> (<=)
    | LT -> (<)
    | GE -> (>=)
    | GT -> (>)
    | Eq -> (=)
    | NEq -> (!=) in
  (* Basic simplification when all values are explicit *)
  let simpl_exprop2 op e1 e2 lv lk =
    match (simpl_expr e1 lv lk), (simpl_expr e2 lv lk) with
      | ExprConst(i1), ExprConst(i2) -> (match (evalfun_of_relop2 op) i1 i2 with
                                           | true -> PropConst(True)
                                           | false -> PropConst(False))
      | es1, es2 -> Rel(op, es1, es2)
in
(* Main formula simplification *)
  match f with
    | PropConst(op) -> PropConst(op)
    | PropUn(op, f) -> simpl_propop1 op f lv lk
    | PropBin(op, f1, f2) -> simpl_propop2 op f1 f2 lv lk
    | Rel(op, e1, e2) -> simpl_exprop2 op e1 e2 lv lk
    | FreshState(f) -> FreshState(f) (* TODO *)
  (* TODO: Propagate the explicit parameter values to FreshState *)
  (* TODO: Propagate the simplification to FreshState *)
and
(* Unary operator simplification *)
simpl_propop1 op f lv lk =
  match op with
    | Neg -> match simpl_formula f lv lk with
               | PropConst True -> PropConst False
               | PropConst False -> PropConst True
               | PropUn(Neg, ffs) -> ffs
               | fs -> PropUn(Neg, fs)
and
(* Binary operator simplification *)
simpl_propop2 op f1 f2 lv lk =
  match op with
    | And -> simpl_propop2_And f1 f2 lv lk
    | Or -> simpl_propop2_Or f1 f2 lv lk
    | Impl -> simpl_propop2_Impl f1 f2 lv lk
and
(* AND (/\) operator simplification *)
simpl_propop2_And f1 f2 lv lk =
  match simpl_formula f1 lv lk with
    | PropConst(True) -> simpl_formula f2 lv lk
    | PropConst(False) -> PropConst(False)
    | fs1 -> (match simpl_formula f2 lv lk with
                | PropConst(True) -> fs1
                | PropConst(False) -> PropConst(False)
                | fs2 -> PropBin(And, fs1, fs2))
and
(* OR (\/) operator simplification *)
simpl_propop2_Or f1 f2 lv lk =
  match simpl_formula f1 lv lk with
    | PropConst(True) -> PropConst(True)
    | PropConst(False) -> simpl_formula f2 lv lk
    | fs1 -> (match simpl_formula f2 lv lk with
                | PropConst(True) -> PropConst(True)
                | PropConst(False) -> fs1
                | fs2 -> PropBin(Or, fs1, fs2))
and
(* IMPLIES (=>) operator simplification *)
simpl_propop2_Impl f1 f2 lv lk =
  match simpl_formula f1 lv lk with
    | PropConst(True) -> simpl_formula f2 lv lk
    | PropConst(False) -> PropConst(True)
    | fs1 -> (match simpl_formula f2 lv lk with
                | PropConst(True) -> PropConst(True)
                | PropConst(False) -> PropUn(Neg, fs1)
                | fs2 -> PropBin(Impl, fs1, fs2)) ;;

(** Return the simplified formula and the simplification hypotheses in a couple *)
(* TODO: Refactor using a fold_left-like function *)
let couple_simplify f lv lk =
  let rec iter_lv l =
    match l with
      | [] -> PropConst(True)
      | (v, i) :: [] -> Rel(Eq, ExprVar(v), ExprConst(i))
      | (v, i) :: t -> PropBin(And,  Rel(Eq, ExprVar(v), ExprConst(i)), iter_lv t) in
  let rec iter_lk l =
    match l with
      | [] -> PropConst(True)
      | ((v, lm), i) :: [] -> Rel(Eq, ExprParam(v, lm), ExprConst(i))
      | ((v, lm), i) :: t -> PropBin(And,  Rel(Eq, ExprParam(v, lm), ExprConst(i)), iter_lk t)
in
  (simpl_formula (PropBin(And, iter_lv lv, iter_lk lk)) [] [], simpl_formula f lv lk) ;;

(** Return the simplified formula and the simplification hypotheses as a conjunction *)
let simplify f lv lk =
  let (a, b) = couple_simplify f lv lk
in
  PropBin(And, a, b) ;;



(********************************************)
(*** Grammar for imperative path programs ***)
(********************************************)

(** Grammar for imperative programs *)
type prog =
  | Skip
  | Set of var * int
  | Incr of var
  | Decr of var
  | Seq of prog * prog
  | If of formula * prog * prog
  | While of formula * formula * prog
  | Forall of prog * prog
  | Exists of prog * prog
  | Assert of formula ;;

(** Convert an imperative program into a string for pretty-printing with indentation *)
let string_of_prog_indent p =
  let nli indent = ("\n" ^ (String.make indent ' ')) in
  let newindent = 2 in
  let rec string_of_prog_indentation p indent =
    match p with
      | Skip -> "SKIP"
      | Incr(v) -> ((string_of_var v) ^ "+")
      | Decr(v) -> ((string_of_var v) ^ "−")
      | Set(v, k) -> ((string_of_var v) ^ " := " ^ (string_of_int k))
      | Seq(p1, p2) -> ((string_of_prog_indentation p1 indent) ^ " ;"
        ^ (nli indent) ^ (string_of_prog_indentation p2 indent))
      | If(c, p1, p2) -> ("IF " ^ (string_of_formula c) ^ " THEN"
        ^ (nli (indent + newindent)) ^ (string_of_prog_indentation p1 (indent + newindent))
        ^ (nli indent) ^ "ELSE"
        ^ (nli (indent + newindent)) ^ (string_of_prog_indentation p2 (indent + newindent))
        ^ (nli indent) ^ "END IF")
      | While(c, i, p) -> ("WHILE " ^ (string_of_formula c) ^ " WITH " ^ (string_of_formula i) ^ " DO"
        ^ (nli (indent + newindent)) ^ (string_of_prog_indentation p (indent + newindent))
        ^ (nli indent) ^ "END WHILE")
      | Forall(p1, p2) -> ("∀(" ^ (string_of_prog_indentation p1 indent) ^ ", " ^ (string_of_prog_indentation p2 indent) ^ ")")
      | Exists(p1, p2) -> ("∃(" ^ (string_of_prog_indentation p1 indent) ^ ", " ^ (string_of_prog_indentation p2 indent) ^ ")")
      | Assert(f) -> ("assert(" ^ (string_of_formula f) ^ ")")
in
  string_of_prog_indentation p 0 ;;

(** Convert an imperative program into a string for one-line pretty-printing *)
let rec string_of_prog = function
  | Skip -> "SKIP"
  | Incr(v) -> ((string_of_var v) ^ "+")
  | Decr(v) -> ((string_of_var v) ^ "−")
  | Set(v, k) -> ((string_of_var v) ^ " := " ^ (string_of_int k))
  | Seq(p1, p2) -> ((string_of_prog p1) ^ " ; " ^ (string_of_prog p2))
  | If(c, p1, p2) -> ("(IF " ^ (string_of_formula c) ^ " THEN " ^ (string_of_prog p1) ^ " ELSE " ^ (string_of_prog p2) ^ " END IF)")
  | While(c, i, p) -> ("(WHILE " ^ (string_of_formula c) ^ " WITH " ^ (string_of_formula i) ^ " DO " ^ (string_of_prog p) ^ " END WHILE)")
  | Forall(p1, p2) -> ("∀(" ^ (string_of_prog p1) ^ ", " ^ (string_of_prog p2) ^ ")")
  | Exists(p1, p2) -> ("∃(" ^ (string_of_prog p1) ^ ", " ^ (string_of_prog p2) ^ ")")
  | Assert(f) -> ("assert(" ^ (string_of_formula f) ^ ")") ;;



(************************************************)
(*** Predicate calculus: weakest precondition ***)
(************************************************)

(** PHI formulas (resources of a variable) *)
let phi v omega =
  let rec phi_eval v l omega =
    match l with
      | [] -> PropConst True
      | h :: [] -> if (mem h omega)
                   then formula_of_multformula (getformula h)
                   else PropUn(Neg, formula_of_multformula (getformula h))
      | h :: t -> PropBin(And, (if (mem h omega)
                                then formula_of_multformula (getformula h)
                                else PropUn(Neg, formula_of_multformula (getformula h))),
                               phi_eval v t omega)
in
  phi_eval v (getpredec v) omega ;;
let rec conj_phi_param v l comp =
  match l with
    | [] -> raise (Invalid_argument "conj_phi_param: Empty powerset (should contain at least [])")
    | h :: [] -> PropBin(Impl, phi v h, Rel(comp, ExprParam(v, h), ExprVar v))
    | h :: t -> PropBin(And, PropBin(Impl, phi v h, Rel(comp, ExprParam(v, h), ExprVar v)), conj_phi_param v t comp) ;;
let phi_incr v = conj_phi_param v (powerset (getpredec v)) GT ;;
let phi_decr v = conj_phi_param v (powerset (getpredec v)) LT ;;

(** Weakest precondition *)
let rec wp prog post =
  let incr_formula v = ExprBin(Plus, ExprVar v, ExprConst 1) in
  let decr_formula v = ExprBin(Minus, ExprVar v, ExprConst 1)
in
  match prog with
    | Skip -> post
    | Incr v -> PropBin(And, phi_incr v, replace post v (incr_formula v))
    | Decr v -> PropBin(And, phi_decr v, replace post v (decr_formula v))
    | Set(v, k) -> replace post v (ExprConst(k))
    | Seq(p1, p2) -> wp p1 (wp p2 post)
    | If(c, p1, p2) -> PropBin(And, PropBin(Impl, c, wp p1 post),
                                     PropBin(Impl, PropUn(Neg, c), wp p2 post))
    | While(c, i, p) -> PropBin(And, i, PropBin(And, FreshState(PropBin(Impl, PropBin(And, i, c), wp p i)),
                                                      FreshState(PropBin(Impl, PropBin(And, i, PropUn(Neg, c)), post))))
    | Forall(p1, p2) -> PropBin(And, wp p1 post, wp p2 post)
    | Exists(p1, p2) -> PropBin(Or, wp p1 post, wp p2 post)
    | Assert(f) -> PropBin(And, f, post) ;;



(****************************************************************)
(*** Translation of a formula into an ASP program for solving ***)
(****************************************************************)

(* This consists in creating a whole ASP program where each node of the formula tree is modeled by an ASP atom
while variables and parameters are represented by ASP variables. *)

(** Translate a formula into ASP *)
let write_asp out f =
  let rec power x i = if i = 0 then 1 else x * power x (i - 1) in
  let rec from_up_to n m = if n > m then [] else n :: (from_up_to (n + 1) m) in
  let asp_of_state_var s v = (if s = 0 then "" else "S" ^ (string_of_int s) ^ "X") ^ (asp_of_var v) in
  let asp_of_state_param p = let (v, n) = p in "K_" ^ (asp_of_var v) ^ "_" ^ (string_of_int n) in
  let param_enum_var v = fold_right (fun n l -> (v, n) :: l) (from_up_to 0 ((power 2 (length (getpredec v))) - 1)) [] in
  let param_enum = fold_right (fun v l -> (param_enum_var v) @ l) varlist [] in
  let state_enum s = ((if s = 0 then "main_" else "") ^ "state(" ^ (string_of_list (asp_of_state_var s) "," varlist) ^ ")") in
  let soft_state_enum s = if s = 0 then "" else ((state_enum s) ^ ", ") in
  let params_enum = ("params(" ^ (string_of_list asp_of_state_param "," param_enum) ^ ")") in
  let iatom = ref (-1) in
  let nextatom s = iatom := !iatom + 1 ; "prop" ^ (string_of_int !iatom) ^
                                          (if s = 0 then "" else "(" ^ (string_of_list (asp_of_state_var s) "," varlist) ^ ")") in
  let istate = ref (0) in
  let nextstate () = istate := !istate + 1 ; !istate in
  let rec write_atom_state out f s =
    match f with
      | PropConst(op) ->
        (match op with
          | True -> "t"
          | False -> "f")
      | PropUn(op, f1) ->
        (let ncurrent = nextatom s in
        let nf1 = write_atom_state out f1 s in
          (match op with
            | Neg -> output_string out (ncurrent ^ " :- " ^ (soft_state_enum s) ^ "not " ^ nf1 ^ ".\n")
          ) ; ncurrent)
      | PropBin(op, f1, f2) -> (
        let ncurrent = nextatom s in
        let nf1 = write_atom_state out f1 s in
        let nf2 = write_atom_state out f2 s in
          (match op with
            | And -> output_string out (ncurrent ^ " :- " ^ (soft_state_enum s) ^ nf1 ^ ", " ^ nf2 ^ ".\n")
            | Or -> output_string out (ncurrent ^ " :- " ^ (soft_state_enum s) ^ nf1 ^ ".\n" ^
                                       ncurrent ^ " :- " ^ (soft_state_enum s) ^ nf2 ^ ".\n")
            | Impl -> output_string out (ncurrent ^ " :- " ^ (soft_state_enum s) ^ nf2 ^ ".\n" ^
                                         ncurrent ^ " :- " ^ (soft_state_enum s) ^ "not " ^ nf1 ^ ".\n")
          ) ; ncurrent)
      | Rel(op, e1, e2) ->
        (let ncurrent = nextatom s in
          output_string out (ncurrent ^ " :- " ^ (state_enum s) ^ ", " ^ params_enum ^ ", " ^
                             (asp_of_state_expr s e1) ^ " " ^ (asp_of_relop2 op) ^ " " ^ (asp_of_state_expr s e2) ^ ".\n")
          ; ncurrent)
      | FreshState(f) ->
        (let ncurrent = nextatom s in
        let nf1 = nextatom s in
        let ns = nextstate () in
        let nf2 = write_atom_state out f ns in
          output_string out (ncurrent ^ " :- not " ^ nf1 ^ ".\n") ;
          output_string out (nf1 ^ " :- " ^ (state_enum ns) ^ ", not " ^ nf2 ^ ".\n")
          ; ncurrent) in
  let write_atoms out f =
    output_string out ("main_state :- " ^ write_atom_state out f 0 ^ ".\n") ;
    output_string out "\nt.\n:- f.\n\n:- not main_state.\n" in
  let write_header out =
    iter (fun v -> output_string out ("varbound_" ^ (asp_of_var v) ^ "(0.." ^ (string_of_int (getbound v)) ^ ").\n")) varlist ;
    output_string out "\n" ;
    iter (fun v -> output_string out ("1 {var_" ^ (asp_of_var v) ^ "(X) : varbound_" ^ (asp_of_var v) ^ "(X)} 1.\n")) varlist ;
    output_string out "\n" ;
    iter (fun v ->
      (iter
        (fun n -> output_string out ("1 {param_" ^ (asp_of_var v) ^ "_" ^ (string_of_int n) ^ "(X) : varbound_" ^ (asp_of_var v) ^ "(X)} 1.\n"))
        (from_up_to 0 ((power 2 (length (getpredec v))) - 1))
      )) varlist ;
    output_string out "\n" ;

    output_string out ((state_enum 0) ^ " :- ") ;
    output_string out (string_of_list (fun v -> "var_" ^ (asp_of_var v) ^ "(" ^ (asp_of_var v) ^ ")") ", " varlist) ;
    output_string out ".\n\n" ;

    output_string out ("state(" ^ (string_of_list asp_of_var "," varlist) ^ ") :- ") ;
    output_string out (string_of_list (fun v -> "varbound_" ^ (asp_of_var v) ^ "(" ^ (asp_of_var v) ^ ")") ", " varlist) ;
    output_string out ".\n\n" ;

    output_string out (params_enum ^ " :- ") ;
    output_string out (string_of_list
      (fun c -> let (v, n) = c in "param_" ^ (asp_of_var v) ^ "_" ^ (string_of_int n) ^ "(K_" ^ (asp_of_var v) ^ "_" ^ (string_of_int n) ^ ")")
      ", " param_enum) ;
    output_string out ".\n\n" in
  let write_footer out =
    output_string out ("\n%#hide.\n#show main_state/" ^ (string_of_int (length varlist)) ^
                       ".\n#show params/" ^ (string_of_int (length param_enum)) ^ ".\n\n")
in
  write_header out ; write_atoms out f ; write_footer out ;;

(** Translate a formula into an ASP program and write it into a file *)
let write_example f file =
  let out = open_out file
in
  write_asp out f ;
  close_out out ;;

(** Print the correspondence between parameters and ASP variables *)
let asp_params () =
  let paramlist =
    fold_right (fun v l -> (powerset_sort (getpredec v) multcompare) :: l) varlist []
in
  iter (fun v ->
    iter (fun lp -> print_endline ((string_of_expr (ExprParam(v, lp))) ^ " ..... " ^ (asp_of_expr (ExprParam(v, lp)))))
    (nth paramlist (index varlist v))) varlist ;;
(*  iter (fun n -> print_string ((string_of_expr (ExprParam(fst n, snd n))) ^ " ..... " ^ (asp_of_expr (ExprParam(fst n, snd n))))) paramlist ;; *)



(********************************************)
(*** Sandbox: apply processes to examples ***)
(********************************************)

(** Toy example *)
(*
let prog1 = Seq(Incr "A", Decr "A") ;;
let post1 = Rel(Eq, ExprVar "A", ExprConst 0) ;;
let pre1 = wp prog1 post1 ;;
print_endline "*****************************************************" ;;
print_endline "*** Toy example: negative feedback with Incr/Decr ***" ;;
print_endline "*****************************************************" ;;
print_endline "*** Program ***" ;;
print_endline (string_of_prog_indent prog1) ;;
print_endline "*** Postcondition ***" ;;
print_endline (string_of_formula post1) ;;
print_endline "*** Raw weakest precondition ***" ;;
print_endline (string_of_formula pre1) ;;
write_example pre1 "toyex-incrdecr.lp" ;;
(* OK: One solution: A = 0, k_a,{} = 0, k_a,{m} = 1  *)

let prog2 = While(PropConst True, Rel(Eq, ExprVar "A", ExprConst 0), Seq(Incr "A", Decr "A")) ;;
let post2 = PropConst True (* Rel(Eq, ExprVar A, ExprConst 0) *) ;;
let pre2 = wp prog2 post2 ;;
print_endline "******************************************************" ;;
print_endline "*** Toy example: Negative feedback with While loop ***" ;;
print_endline "******************************************************" ;;
print_endline "*** Program ***" ;;
print_endline (string_of_prog_indent prog2) ;;
print_endline "*** Postcondition ***" ;;
print_endline (string_of_formula post2) ;;
print_endline "*** Raw Weakest precondition ***" ;;
print_endline (string_of_formula pre2) ;;
write_example pre2 "toyex-while.lp" ;;
(* OK: One solution: A = 0, k_a,{} = 0, k_a,{m} = 1  *)

let prog3 = If(Rel(Eq, ExprVar "A", ExprConst 1),
  While(PropConst True, Rel(Eq, ExprVar "A", ExprConst 1), Seq(Decr "A", Incr "A")),
  While(PropConst True, Rel(Eq, ExprVar "A", ExprConst 0), Seq(Incr "A", Decr "A"))) ;;
let post3 = PropConst True (* Rel(Eq, ExprVar A, ExprConst 0) *) ;;
let pre3 = wp prog3 post3 ;;
print_endline "*************************************" ;;
print_endline "*** Toy example: Complex feedback ***" ;;
print_endline "*************************************" ;;
print_endline "*** Program ***" ;;
print_endline (string_of_prog_indent prog3) ;;
print_endline "*** Postcondition ***" ;;
print_endline (string_of_formula post3) ;;
print_endline "*** Raw weakest precondition ***" ;;
print_endline (string_of_formula pre3) ;;
write_example pre3 "toyex-complex.lp" ;;
(* OK: Two solutions: A = 0/1, k_a,{} = 0, k_a,{m} = 1  *)
*)



(** Main article's toy example *)

let prog1 = Seq(Seq(Incr "b", Incr "c"), Decr "b") ;;
let post1 = Rel(Eq, ExprVar "b", ExprConst 0) ;;
let pre1_ns = PropBin(And, wp prog1 post1, Rel(Eq, ExprVar "a", ExprConst 1)) ;; (* Old-fashioned refining without simplification *)
let pre1 = simplify (wp prog1 post1) [("a", 1) ; ("b", 0) ; ("c", 0)] [] ;; (* With refining and simplification *)
print_endline "******************************" ;;
print_endline "*** Example 1 from article ***" ;;
print_endline "******************************" ;;
print_endline "*** Program ***" ;;
print_endline (string_of_prog_indent prog1) ;;
print_endline "*** Postcondition ***" ;;
print_endline (string_of_formula post1) ;;
print_endline "*** Refined weakest precondition without simplification ***" ;;
print_endline (string_of_formula pre1_ns) ;;
print_endline "*** Refined and simplified weakest precondition ***" ;;
print_endline (string_of_formula pre1) ;;
write_example pre1_ns "simpleex1_ns.lp" ;;
write_example pre1 "simpleex1.lp" ;;
(* OK: 16 solutions (with refining) *)
(* Constraints: a = 1, b = 0, c = 0, k_b,{σ} = 0, k_b,{λ;σ} = 1, k_c,{l} = 1 *)

let prog2 = Seq(Incr "b", Decr "b") ;;
let post2 = post1 ;;
let pre2 = wp prog2 post2 ;;
print_endline "******************************" ;;
print_endline "*** Example 2 from article ***" ;;
print_endline "******************************" ;;
print_endline "*** Program ***" ;;
print_endline (string_of_prog_indent prog2) ;;
print_endline "*** Postcondition ***" ;;
print_endline (string_of_formula post2) ;;
print_endline "*** Raw weakest precondition ***" ;;
print_endline (string_of_formula pre2) ;;
write_example pre2 "simpleex2.lp" ;;
(* OK: No solution *)

let prog3 = Incr "c" ;;
let post3 = post1 ;;
let pre3 = wp prog3 post3 ;;
print_endline "******************************" ;;
print_endline "*** Example 3 from article ***" ;;
print_endline "******************************" ;;
print_endline "*** Program ***" ;;
print_endline (string_of_prog_indent prog3) ;;
print_endline "*** Postcondition ***" ;;
print_endline (string_of_formula post3) ;;
print_endline "*** Raw weakest precondition ***" ;;
print_endline (string_of_formula pre3) ;;
write_example pre3 "simpleex3.lp" ;;
(* OK: 128 solutions *)

let prog4 = While(Rel(LT, ExprVar "b", ExprConst 1), PropConst True, Exists(Exists(Exists(Incr "b", Decr "b"), Incr "b"), Decr "c")) ;;
let post4 = Rel(Eq, ExprVar "b", ExprConst 1) ;;
let pre4_oldr = PropBin(And, wp prog4 post4, PropBin(And, PropBin(And, PropBin(And, PropBin(And, PropBin(And, (* Old-fashioned refining without simplification *)
  Rel(Eq, ExprVar "a", ExprConst 1), Rel(Eq, ExprVar "b", ExprConst 0)), Rel(Eq, ExprVar "c", ExprConst 0)),
  Rel(Eq, ExprParam("b", ["sigma"]), ExprConst 0)), Rel(Eq, ExprParam("b", ["lambda" ; "sigma"]), ExprConst 0)), Rel(Eq, ExprParam("c", ["l"]), ExprConst 0))) ;;
let pre4 = simplify (wp prog4 post4) [("a", 1) ; ("b", 0) ; ("c", 0)] [(("b", ["sigma"]), 0) ; (("b", ["lambda" ; "sigma"]), 0) ; (("c", ["l"]), 0)] ;; (* With refining and simplification *)
print_endline "**********************************************************" ;;
print_endline "*** Example 4 from article: While loop with I ≡ (True) ***" ;;
print_endline "**********************************************************" ;;
print_endline "*** Program ***" ;;
print_endline (string_of_prog_indent prog4) ;;
print_endline "*** Postcondition ***" ;;
print_endline (string_of_formula post4) ;;
print_endline "*** Raw weakest precondition ***" ;;
print_endline (string_of_formula pre4_oldr) ;;
print_endline "*** Refined and simplified weakest precondition ***" ;;
print_endline (string_of_formula pre4) ;;
write_example pre4 "simpleex4.lp" ;;
(* OK: No solution (with refining) *)

let prog4b = While(Rel(LT, ExprVar "b", ExprConst 1), Rel(Eq, ExprVar "a", ExprConst 1), Exists(Exists(Exists(Incr "b", Decr "b"), Incr "c"), Decr "c")) ;;
let post4b = Rel(Eq, ExprVar "b", ExprConst 1) ;;
let pre4b_oldr = PropBin(And, wp prog4b post4b, PropBin(And, PropBin(And, PropBin(And, PropBin(And, PropBin(And, (* Old-fashioned refining without simplification *)
  Rel(Eq, ExprVar "a", ExprConst 1), Rel(Eq, ExprVar "b", ExprConst 0)), Rel(Eq, ExprVar "c", ExprConst 0)),
  Rel(Eq, ExprParam("b", ["sigma"]), ExprConst 0)), Rel(Eq, ExprParam("b", ["lambda" ; "sigma"]), ExprConst 0)), Rel(Eq, ExprParam("c", ["l"]), ExprConst 0))) ;;
let pre4b = simplify (wp prog4b post4b) [("a", 1) ; ("b", 0) ; ("c", 0)] [(("b", ["sigma"]), 0) ; (("b", ["lambda" ; "sigma"]), 0) ; (("c", ["l"]), 0)] ;; (* With refining and simplification *)
print_endline "***********************************************************" ;;
print_endline "*** Example 4 from article: While loop with I ≡ (a = 1) ***" ;;
print_endline "***********************************************************" ;;
print_endline "*** Program ***" ;;
print_endline (string_of_prog_indent prog4b) ;;
print_endline "*** Postcondition ***" ;;
print_endline (string_of_formula post4b) ;;
print_endline "*** Raw weakest precondition ***" ;;
print_endline (string_of_formula pre4b_oldr) ;;
print_endline "*** Refined and simplified weakest precondition ***" ;;
print_endline (string_of_formula pre4b) ;;
write_example pre4b "simpleex4b.lp" ;;
(* OK : Aucune solution (avec raffinage) *)

let prog5 = Seq(Seq(Seq(Incr "b", Set("c", 1)), Assert(PropBin(And, Rel(Eq, ExprVar "b", ExprConst 1), Rel(Eq, ExprVar "a", ExprConst 1)))), Decr "b") ;;
let post5 = PropConst True ;;
(* let pre4b = wp prog4b post4b ;; (* Sans raffinage *) *)
let pre5 = simplify (wp prog5 post5) [(* (A, 1) ; *) ("b", 0) ; ("c", 0)] [] ;; (* With refining and simplification *)
print_endline "*************************************************" ;;
print_endline "*** Example 5: Simple tests on Set and Assert ***" ;;
print_endline "*************************************************" ;;
print_endline "*** Program ***" ;;
print_endline (string_of_prog_indent prog5) ;;
print_endline "*** Postcondition ***" ;;
print_endline (string_of_formula post5) ;;
print_endline "*** Simplified weakest precondition ***" ;;
print_endline (string_of_formula pre5) ;;
write_example pre5 "simpleex5.lp" ;;



(** Lambda phage (from Adrien Richard's PhD thesis) *)
(*
(*Qa = N+; CII+; CI+; CI+; N−; CII−
Ra ≡ (CI = 2 ∧ Cro = 0 ∧ CII = 0 ∧ N = 0)*)

let progpl1 = Seq(Seq(Seq(Seq(Seq(Incr "N", Incr "CII"), Incr "CI"), Incr "CI"), Decr "N"), Decr "CII") ;;
(* (* Made before any simplification was possible *)
let postpl1 = PropBin(And, PropBin(And, PropBin(And, Rel(Eq, ExprVar CI, ExprConst 2), Rel(Eq, ExprVar Cro, ExprConst 0)),
                                                   Rel(Eq, ExprVar CII, ExprConst 0)), Rel(Eq, ExprVar N, ExprConst 0)) ;;
let prepl1 = wp progpl1 postpl1 ;;
*)

let postpl1 = PropConst True ;;
let prepl1_nr = wp progpl1 postpl1 ;; (* Without refining *)
let prepl1 = simplify (wp progpl1 postpl1) [("CI", 0) ; ("Cro", 0) ; ("CII", 0) ; ("N", 0)] [] ;; (* With refining and simplification *)

print_endline "*********************************" ;;
print_endline "*** Example 1 on lambda phage ***" ;;
print_endline "*********************************" ;;
print_endline "* Program *" ;;
print_endline (string_of_prog_indent progpl1) ;;
print_endline "* Postcondition *" ;;
print_endline (string_of_formula postpl1) ;;
print_endline "* Raw weakest precondition *" ;;
print_endline (string_of_formula prepl1_nr) ;;
print_endline "* Simplified weakest precondition *" ;;
print_endline (string_of_formula prepl1) ;;
print_endline "***************************" ;;

write_example prepl1_nr "phagelex1_nr.lp" ;;
write_example prepl1 "phagelex1.lp" ;;
(* Bad: combinatorial explosion => too many results *)
*)



(** Circadian cycle (Circlock project) *)
(*
let prog1 = Skip ;;
let post1 = PropConst(True) ;;
let pre1 = wp prog1 post1 ;;

print_endline "***********************" ;;
print_endline "*** Circadian cycle ***" ;;
print_endline "***********************" ;;
print_endline "* Program *" ;;
print_endline (string_of_prog_indent prog1) ;;
print_endline "* Postcondition *" ;;
print_endline (string_of_formula post1) ;;
print_endline "* Raw weakest precondition *" ;;
print_endline (string_of_formula pre1) ;;
print_endline "***************************" ;;

write_example pre1 "circlock1.lp" ;;
*)



(**********************)
(*** Common sandbox ***)
(**********************)

print_endline "***************" ;;
print_endline "*** Sandbox ***" ;;
print_endline "***************" ;;

let testf = PropUn(Neg, PropUn(Neg, Rel(LT, ExprVar "A", ExprConst 1))) ;;
let testf_simpl = simplify testf [] [] ;;
print_endline "*** Formula without simplification ***" ;;
print_endline (string_of_formula testf) ;;
print_endline "*** Formula with simplification ***" ;;
print_endline (string_of_formula testf_simpl) ;;

print_endline "*** Formula 4b simplified again ***" ;;
print_endline (string_of_formula (simplify pre4b [] [])) ;;



(**************************************************)
(*** Correspondences ASP variables / parameters ***)
(**************************************************)
(* Uncomment to print the ASP representation corresponding to each parameter: *)
(*
print_endline "   *****" ; asp_params () ;;
*)

