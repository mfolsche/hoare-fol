#!/bin/sh

for fichier in $(ls *.lp)
do
  clingo 0 --verbose=0 ${fichier} > ${fichier}$1.out
done
